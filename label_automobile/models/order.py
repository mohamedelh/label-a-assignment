from sqlalchemy import Column, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from label_automobile.models.meta import Base, BaseModel


class Order(BaseModel, Base):
    __tablename__ = 'order'
    # Add a one-to-many relationship with Orderlist
    children = relationship("Orderlist")

    parent_email = Column(String, ForeignKey("user.email_"), unique=True)
    delivery_date_time = Column(DateTime, nullable=True)
