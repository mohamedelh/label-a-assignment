from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.dialects.postgresql import UUID

from label_automobile.models.meta import Base, BaseModel


class Orderlist(BaseModel, Base):

    __tablename__ = 'order_list'

    parent_order_email = Column(String, ForeignKey("order.parent_email"))
    quantity = Column(Integer, nullable=False, default=1)
    product_name = Column(String, ForeignKey("product.name"), nullable=False)



