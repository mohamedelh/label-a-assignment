from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from label_automobile.models.meta import Base, BaseModel


class Product(BaseModel, Base):
    __tablename__ = 'product'

    name = Column(String, unique=True, nullable=False)
    details = Column(String, nullable=False, default="To-do")
