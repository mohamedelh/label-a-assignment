import datetime
from label_automobile.models.order import Order
from label_automobile.models.orderlist import Orderlist


class OrderRepository:
    def __init__(self, session):
        self.session = session
        self.model = Order

    def create(self, email):
        """
        Create an order for the customer
        :param email: str a customer's email
        """
        order = Order()
        order.parent_email = email
        self.session.add(order)

    def destroy(self, email):
        order = self.session.query(Order).filter(Order.parent_email == email).one()
        self.session.delete(order)
        order_list = self.session.query(Orderlist).filter(Order.parent_email == email).all()
        if order_list is not None:
            for i in order_list:
                self.session.delete(i)

    def get_order(self, email):
        """
        Create an order for the customer
        :param email: str a customer's email
        :return: Order
        """
        return self.session.query(Order).filter(Order.parent_email == email).one()

    def add_product(self, order):
        """
        Add a product with the desired quantity to orderlist. If product already exists, adjust quantity.
        :param order: Order an order for a specific product
        """
        order_list = self.session.query(Orderlist).filter(
            Orderlist.parent_order_email == order.email and Orderlist.product_name == order.product)

        if order_list.scalar() is not None:
            order_list = order_list.one()
            order_list.quantity += order.quantity
        else:
            order_list = Orderlist()
            order_list.parent_order_email = order.email
            order_list.product_name = order.product_name
            order_list.quantity = order.quantity
            self.session.add(order_list)

    def delete_product(self, order):
        """
        Delete the specified amount of quantity from order. If quantity is 0 or lower, remove the order completely.
        :param order: Order an order for a specific product
        """
        order_list = self.session.query(Orderlist).filter(
            Orderlist.parent_order_email == order.email and Orderlist.product_name == order.product).one()
        order_list.quantity -= order.quantity
        if order_list.quantity <= 0:
            self.session.delete(order_list)

    def set_time(self, email, year, month, day, hour, minute):
        """
         Set delivery time for the order
         :param email: str a customer's email
         :param year: int the given year
         :param month int the given month
         :param day: int the given day
         :param hour: str the given hour
         :param minute: int the given minute

         """
        order = self.session.query(Order).filter(Order.parent_email == email).one()
        order.delivery_date_time = datetime.datetime(year, month, day, hour, minute, 0, 0)
