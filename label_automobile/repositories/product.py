from label_automobile.helpers.exceptions import NotFoundError
from label_automobile.models.product import Product


class ProductRepository:
    def __init__(self, session):
        self.session = session
        self.model = Product

    def find_by_name(self, name):
        """
        Find product by name.
        :param name: str name of the product
        :return: Product
        """
        product = self.session.query(Product).filter(Product.name == name)
        if product.scalar() is not None:
            return product.one()
        else:
            raise NotFoundError

    def add(self, product):
        """
        Add a product
        :param product: Product the product with it's description
        """
        self.session.add(product)

    def delete(self, product):
        """
        Delete a product
        :param product: Product the product with it's description
        """
        self.session.delete(product)
