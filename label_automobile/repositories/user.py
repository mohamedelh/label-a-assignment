from label_automobile.helpers.exceptions import NotFoundError
from sqlalchemy import update

from label_automobile.models.user import User


class UserRepository:
    def __init__(self, session):
        self.session = session
        self.model = User

    def find_by_email(self, email):
        """
        Find customer by email.
        :param email: str email of the customer
        :return: User
        """
        user = self.session.query(User).filter(User.email == email)
        if user.scalar() is not None:
            return user.one()
        else:
            raise NotFoundError("Not found")

    def login(self, user):
        """
        Log customer in
        :param user: User the customer
        """
        user.active = True

    def logout(self, user):
        """
        Log customer out
        :param user: User the customer
        """
        user.active = False

    def check_user(self, inserted_email, password):
        """
        Check if inserted customer exists.
        :param password: str the password inserted by customer
        :param inserted_email: str the email inserted by customer

        """
        found_user = self.session.query(User).filter(
            User.email_ == inserted_email and
            User.password == password
        )
        if found_user.scalar() is None:
            raise NotFoundError
        else:
            return found_user.one()

    def add(self, user):
        """
        Add a new customer
        :param user: User the new customer
        """
        self.session.add(user)

    def delete(self, user):
        """
        Delete a customer
        :param user: User the existing customer which is going to be deleted.
        """
        self.session.delete(user)
