class NotFoundError(Exception):
    def __init__(self, message):
        super().__init__(message)


class AuthorizationError(Exception):
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors


class WrongInputError(Exception):
    def __init__(self, message):
        super().__init__(message)
