from cornice.resource import view
from psycopg2._psycopg import IntegrityError
from pyramid.httpexceptions import exception_response

from label_automobile.helpers.exceptions import NotFoundError
from label_automobile.repositories.product import ProductRepository
from label_automobile.models import Product


class ProductView:

    def __init__(self, request, context=None):
        self.request = request
        self.session = request.dbsession
        self._repository = ProductRepository(self.session)

    def collection_get(self):
        """
        Get all products available
        :return: list of JSON data
        """
        return [{
            "name": product.name
        } for product in self.session.query(Product).all()]

    @view(validators=('validate_existing_product'))
    def get(self):
        """
        Get a specified product
        :return: JSON
        """
        product = self.request.validated['product']
        return {
            "name": product.name,
            "details": product.details
        }

    @view(validators=('validate_new_product'))
    def collection_post(self):
        """
        Add a new product
        """
        self._repository.add(self.request.validated['product'])

    @view(validators=('validate_existing_product'))
    def delete(self):
        """
        Delete existing product
        """
        self._repository.delete(self.request.validated['product'])

    def validate_new_product(self, request, **kw):
        """
        Validate whether product can be created
        """
        try:
            product = Product()
            product.name = self.request.json['name']
            product.details = self.request.json['details']
            request.validated['product'] = product
        except KeyError:
            raise exception_response(400)
        except IntegrityError:
            raise exception_response(409)

    def validate_existing_product(self, request, **kw):
        """
        Validate if product exists
        """
        try:
            product = self._repository.find_by_name(str(self.request.matchdict['item']))
            request.validated['product'] = product
        except NotFoundError:
            raise exception_response(404)
