from cornice.resource import view
from label_automobile.helpers.exceptions import NotFoundError
from pyramid.httpexceptions import exception_response

from label_automobile.repositories.order import OrderRepository
from label_automobile.models import Orderlist


class OrderView:

    def __init__(self, request, context=None):
        self.request = request
        self.session = request.dbsession
        self._order_repository = OrderRepository(self.session)

    def get(self):
        """
        Get the current order
        :return: list of JSON data
        """
        email = self.request.json['email']
        if str(self.request.matchdict['options']) == "list":
            return [{
                "Product": order_list.product_name,
                "Quantity": order_list.quantity,
            } for order_list in self.session.query(Orderlist).filter(Orderlist.parent_order_email == email).all()]

    def post(self):
        """
        Confirm order
        """
        email = self.request.json['email']
        year = int(self.request.json['year'])
        month = int(self.request.json['month'])
        day = int(self.request.json['day'])
        hour = int(self.request.json['hour'])
        minute = int(self.request.json['minute'])
        if str(self.request.matchdict['options']) == "confirmorder":
            self._order_repository.set_time(email, year, month, day, hour, minute)
            order = self._order_repository.get_order(email)
            return {"ordered": True}

    @view(validators=('validate_product_order'))
    def put(self):
        """
        Add or delete a product from the order
        """
        if str(self.request.matchdict['options']) == "add":
            self._order_repository.add_product(self.request.validated['product_order'])
        if str(self.request.matchdict['options']) == "delete":
            self._order_repository.delete_product(self.request.validatd['product_order'])

    def validate_product_order(self, request, **kw):
        """
        Validate the product order
        """
        try:
            order_list = Orderlist
            order_list.email = self.request.json['email']
            order_list.product_name = self.request.json['product']
            order_list.quantity = int(self.request.json['quantity'])
            self.request.validated['product_order'] = order_list
        except KeyError:
            raise exception_response(400)
        except NotFoundError:
            raise exception_response(404)
