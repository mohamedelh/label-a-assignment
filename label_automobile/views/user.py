from cornice.resource import view
from label_automobile.helpers.exceptions import NotFoundError
from psycopg2._psycopg import IntegrityError
from pyramid.httpexceptions import HTTPError, exception_response
from label_automobile.models import User
from label_automobile.repositories.order import OrderRepository
from ..repositories.user import UserRepository


class UserView:

    def __init__(self, request, context=None):
        self.request = request
        self.session = request.dbsession
        self._user_repository = UserRepository(self.session)
        self._order_repository = OrderRepository(self.session)

    def collection_get(self):
        """
        Get all registered customers
        :return: list of JSON data
        """
        return [{
            "name": user.name,
            "surname": user.surname,
            "email": user.email
        } for user in self.session.query(User).all()]

    @view(validators=('validate_existing_user'))
    def get(self):
        """
        Get a specified customer
        :return: JSON
        """
        user = self.request.validated['user']
        return {"name": user.name, "surname": user.surname}

    @view(accept="application/json", validators=('validate_new_user'))
    def collection_post(self):
        """
        Create a new user

        """
        self._user_repository.session.add(self.request.validated['user'])

    @view(validators=('validate_existing_user'))
    def delete(self):
        """
        Delete an existing user
        """
        self._user_repository.session.delete(self.request.validated['user'])

    @view(validators=('validate_authorized_user'))
    def put(self):
        """
        Login or Logout an user and create or remove the order
        """
        if str(self.request.matchdict['options']) == "login":
            self._user_repository.login(self.request.validated['user'])
            self._order_repository.create(self.request.validated['user'].email)

        if str(self.request.matchdict['options']) == "logout":
            self._user_repository.logout(self.request.validated['user'])
            self._order_repository.destroy(self.request.validated['user'].email)

    def validate_new_user(self, request, **kw):
        """
        Validate if new user can be created without conflicts
        """
        try:
            user = User()
            user.name = self.request.json['name']
            user.surname = self.request.json['surname']
            user.email = self.request.json['email']
            user.password = self.request.json['password']
            request.validated['user'] = user
        except KeyError:
            raise exception_response(400)
        except IntegrityError:
            raise exception_response(409)

    def validate_existing_user(self, request, **kw):
        """
        Validate if user exists
        """
        try:
            user = self._user_repository.find_by_email(str(self.request.matchdict['options']))
            request.validated['user'] = user
        except NotFoundError:
            raise exception_response(404)

    def validate_authorized_user(self, request, **kw):
        """
        Validate if user inserted the right email and password
        """
        try:
            user = self._user_repository.check_user(self.request.json['email'], self.request.json['password'])
            request.validated['user'] = user
        except NotFoundError:
            raise exception_response(404)
