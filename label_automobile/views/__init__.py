from .product import ProductView
from .user import UserView
from .order import OrderView
from cornice import Service, resource

def includeme(config):
    user_resource = resource.add_resource(UserView, collection_path='/users', path='/users/{options}')
    product_resource = resource.add_resource(ProductView, collection_path='/products', path='/products/{item}')
    order_resource = resource.add_resource(OrderView, collection_path='/orders', path='/orders/{options}')
    config.add_cornice_resource(user_resource)
